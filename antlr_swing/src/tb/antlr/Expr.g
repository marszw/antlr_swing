grammar Expr;

options {
  output       = AST;
  ASTLabelType = CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

blok
  :
  BEGIN^
  (
    stat
    | blok
  )*
  END!
  ;

prog
  :
  (
    stat
    | blok
  )+
  EOF!
  ;

stat
  :
  logexpr NL
    -> logexpr
  //    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
  | VAR ID NL
    ->
      ^(VAR ID)
  | ID PODST logexpr NL
    ->
      ^(PODST ID logexpr)
  | if_stat NL
    -> if_stat
  | NL
    ->
  ;

if_stat
  :
  IF^ logexpr THEN! NL? blok (ELSE! NL? blok)?
  ;

logexpr
  :
  expr
  (
    DIF^ expr
    | CMP^ expr
  )*
  ;

expr
  :
  multExpr
  (
    PLUS^ multExpr
    | MINUS^ multExpr
  )*
  ;

multExpr
  :
  atom
  (
    MUL^ atom
    | DIV^ atom
  )*
  ;

atom
  :
  INT
  | ID
  | LP! expr RP!
  ;

VAR
  :
  'var'
  ;

CMP
  :
  '=='
  ;

IF
  :
  'if'
  ;

ELSE
  :
  'else'
  ;

THEN
  :
  'then'
  ;

DIF
  :
  '!='
  ;

BEGIN
  :
  '{'
  ;

END
  :
  '}'
  ;

ID
  :
  (
    'a'..'z'
    | 'A'..'Z'
    | '_'
  )
  (
    'a'..'z'
    | 'A'..'Z'
    | '0'..'9'
    | '_'
  )*
  ;

INT
  :
  '0'..'9'+
  ;

NL
  :
  '\r'? '\n'
  ;

WS
  :
  (
    ' '
    | '\t'
  )+
  
  {
   $channel = HIDDEN;
  }
  ;

LP
  :
  '('
  ;

RP
  :
  ')'
  ;

PODST
  :
  '='
  ;

PLUS
  :
  '+'
  ;

MINUS
  :
  '-'
  ;

MUL
  :
  '*'
  ;

DIV
  :
  '/'
  ;
