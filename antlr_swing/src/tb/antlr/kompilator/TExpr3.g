tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer number = 0;
  Integer loopnumber = 0;
}
prog    : (e+=codeblock | e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje> start: <name;separator=\" \n\"> ";

codeblock : ^(BEGIN   (e+=codeblock | e+=expr | d+=decl)* ) -> block(wyr={$e}, deklaracje={$d}) ;
decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}
      
expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st}, p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {globals.checkSymbol($ID.text);} -> podst(id={$ID}, numb={$e2.st})
        | INT  {number++;}          -> int(i={$INT.text},j={number.toString()})
        | ID                       {globals.checkSymbol($ID.text);} -> wczyt(id={$ID})
        | ^(CMP e1=expr e2=expr) -> cmp(e1={$e1.st}, e2={$e2.st})
        | ^(DIF e1=expr e2=expr) {number++;} -> diff(e1={$e1.st}, e2={$e2.st}, nr={number.toString()})
        | ^(IF e1=expr b2=codeblock b3=codeblock?) {number++;}  -> if(cond={$e1.st}, code={$b2.st}, codelse={$b3.st}, nr={number.toString()})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}
    